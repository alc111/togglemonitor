#!/bin/sh
gum style \
	--foreground 212 --border-foreground 212 --border double \
	--align center --width 50 --margin "1 2" --padding "2 4" \
	'Welcome to togglemonitor' 'Change output between monitors'

interno=$(gum input --placeholder "Name of the main monitor:")
externo=$(gum input --placeholder "Name of the secondary monitor:")
resolucion=$(gum input --placeholder "Desired resolution:")
opcion=$(gum choose --limit 1 Main Secondary Both) 

if [ $opcion == "Main" ]
then
	if [ $resolucion == "auto" ]
	then
		xrandr --output $externo --off --output $interno --auto
	else
		xrandr --output $externo --off --output $interno --mode $resolucion
	fi
elif [ $opcion == "Secondary" ]
then
	if [ $resolucion == "auto" ]
        then
	       	xrandr --output $interno --off --output $externo --auto
	else
	       	xrandr --output $interno --off --output $externo --mode $resolucion
	fi
elif [ $opcion == "Both" ]
then
	if [ $resolucion == "auto" ]
        then
	       	xrandr --output $externo --auto --same-as $interno --auto
	else
	       	xrandr --output $externo --mode $resolucion --same-as $interno --mode $resolucion
	fi
else
	echo "La opcion introducida no es valida."
	exit 1
fi
