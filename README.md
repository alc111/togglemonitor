# Togglemonitor

Togglemonitor is a simple script that changes screen projection options when having more than one monitor. Gum shell tool is required, to install it first install golang and then execute:
```
go install github.com/charmbracelet/gum@latest 
```
Then add golang bin directory to your PATH, writing the next line in your .bashrc or .zshrc:
```
export PATH=$PATH:/yourgoroute/bin
```
